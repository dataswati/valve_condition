import pickle

from flask import Flask
from flask import request
from flask import jsonify
import pandas as pd
from sklearn.feature_extraction import DictVectorizer



app = Flask('valve_condition')

@app.route('/ping', methods=['GET'])
def ping():
    return "PONG"




@app.route('/predict', methods=['POST'])
def predict():

    df = pd.read_csv("df_for_eval.csv")
    model_file = 'reglog_model'
        
    with open(model_file, 'rb') as f_in:
        model = pickle.load(f_in)

    selected_features = ['PS2_mean', 'PS2_std_dev', 'PS2_variance', 'PS6_median', 'TS3_mean']

    cycle_id = request.get_json()
    print(cycle_id['id'])

    id = int(cycle_id['id'])
    cycle_data = df.loc[id]
    print("cycle_data ", cycle_data)
    X = cycle_data[selected_features].to_dict()

    dv = DictVectorizer(sparse=False)
    X_vect = dv.fit_transform(X)

    prob = model.predict_proba(X_vect)[0, 1]
    if prob >= 0.5:
        pred = 1
    else:
        pred = 0

    result = {
        'probility':  prob,
        'prediction': pred        
    }
        # 'cycle_data': X_vect.to_dict()
    
    return jsonify(result)


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=9696)