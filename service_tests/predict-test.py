#!/usr/bin/env python
# coding: utf-8

import requests
import sys

cycle_id  = sys.argv[1]
id = int(cycle_id ) - 2000 

#print("id: ", id)
url = 'http://localhost:9696/predict'

id_dict = {'id':id}

#print(cycle_id)

response = requests.post(url, json=id_dict ).json()
print(response)

if response['prediction'] == 1:
    print('the valve condition for the cycle %s is optimal ' % cycle_id)
else:
    print('the valve condition for the cycle %s is not optimal ' % cycle_id)