import sys
sys.path.append('src')

from preprocessing_utils import calculate_time_domain_features
import pandas as pd


def test_calculate_time_domain_features():
    # create test data 
    data = pd.Series([1, 2, 3, 4, 5])
    
    # call the function 
    features = calculate_time_domain_features(data)
    
    # Assertions to check every feature 
    assert features['mean'] == 3.0
    assert features['std_dev'] == data.std(ddof=0)  
    assert features['max_value'] == 5
    assert features['min_value'] == 1
    assert features['range'] == 4
    assert features['variance'] == data.var(ddof=0)
    assert features['median'] == 3.0
