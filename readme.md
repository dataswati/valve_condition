# 1. The valve condition model 


## Environment preparation 

1. download the data from : https://archive.ics.uci.edu/static/public/447/condition+monitoring+of+hydraulic+systems.zip

2. Prepare the data directory structure (We suppose in this readme file that you are using linux OS.)

    $ mkdir data & cd data 

    $ mkdir raw & mkdir processed

    $ cd processed


3. Decompress the zip file, excluding documentation and description files, and copy the 18 text files into the `data/raw` directory. Then, move these files into the `data/processed` directory.

4. Install required packages 

    $ pip install -r requirements.txt

### 1.1. The data preprocessing 

1. Navigate to the notebooks directory :

    $ cd notebooks 

2. Execute the preprocessing notebook 

3. Ensure the output files are stored in the 'data/processed' directory 

### 1.2. Feature Selection

- Based on the articles cited in the data documentation (`docs/documentation`), apply various feature selection methods. Use mutual information to identify the 5 most important features for the target variable, then verify these selections with Pearson correlation between the valve condition variable (y) and the sensor variables (X).

- Execute the `feature_selection` notebook.

**Note 1**: The selected features are already utilized in the `/deployment/predict.py` service. You can modify the `selected_features` list if you wish to change these features.

**Note 2**: Ensure `df_for_modeling.csv` is stored in the `data/for_modeling` directory and `df_for_eval.csv` in the `deployment` directory.

#### Reference Articles:

1. Nikolai Helwig, Eliseo Pignanelli, Andreas Schütze, “Condition Monitoring of a Complex Hydraulic System Using Multivariate Statistics,” in Proc. I2MTC-2015 - 2015 IEEE International Instrumentation and Measurement Technology Conference, paper PPS1-39, Pisa, Italy, May 11-14, 2015. doi: 10.1109/I2MTC.2015.7151267

2. N. Helwig, A. Schütze, “Detecting and compensating sensor faults in a hydraulic condition monitoring system,” in Proc. SENSOR 2015 - 17th International Conference on Sensors and Measurement Technology, oral presentation D8.1, Nuremberg, Germany, May 19-21, 2015. doi: 10.5162/sensor2015/D8.1

3. Tizian Schneider, Nikolai Helwig, Andreas Schütze, “Automatic feature extraction and selection for classification of cyclical time series data,” tm - Technisches Messen (2017), 84(3), 198–206. doi: 10.1515/teme-2016-0072

### 1.3. The Model

1. Create a directory for models (unused models can be stored here). Alternatively, use a tracking server like MLFlow:

$ mkdir models 


2. Execute the modeling notebook.

3. Ensure the `reglog_model` is stored in the `deployment` directory.

# 2. Deployment (Docker Service)

Deploy the machine learning prediction service as follows:

1. Build and run the Docker container:

    $ cd deployment
    $ docker build -t valve .
    $ docker run -t valve -p 9696:9696

2. Test the service at `0.0.0.0:9696/ping`. You should receive `pong` as a response.

# 3. Test Inference Service

Test the prediction capability of the model:

1. Navigate to the `service_tests` directory:

    $ cd ../service_test 

2. Use the `predict-test.py` script to test model predictions:

    $ python predict-test.py "<cycle_id>"
    
Replace `<cycle_id>` with a number between 2001 and 2205.

