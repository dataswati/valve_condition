import numpy as np
import pandas as pd
import os

def calculate_time_domain_features(row):
    """
        Calculate time domain features for a given row of sensor data.

        Parameters:
        - row: pandas Series representing sensor readings

        Returns:
        - A pandas Series containing calculated features.
        
    """
    features = {
        'mean': np.mean(row),
        'std_dev': np.std(row),
        'max_value': np.max(row),
        'min_value': np.min(row),
        'range': np.ptp(row),
        'variance': np.var(row),
        'median': np.median(row)
    }
    return pd.Series(features)


# Get the list of files in the directory
#file_names = os.listdir('../data/raw/')

def import_sensor_data(sensor_attributes):
    """
    Imports and combines data from multiple sensors into a single pandas DataFrame.

    This function reads data from different sensors from text files, applies a processing
    function to each row of the read data, and combines all data into a single DataFrame.
    Column names in the final DataFrame are dynamically generated based on the sensor names
    and the specified number of attributes for each sensor.

    Parameters:
    - sensor_attributes (dict): A dictionary where keys are sensor names (str) and values
    are the number of attributes (int) for each sensor.

    Returns:
    - combined_df (DataFrame): A pandas DataFrame containing all combined sensor data, with
    dynamically named columns and time domain features calculated for each row.
    """

    # Initialize an empty DataFrame to store the combined data
    combined_df = pd.DataFrame()

    # Loop through each sensor file
    for sensor_name, attributes_nb in sensor_attributes.items():

        print(sensor_name)
        #print(attributes_nb)        
        
        # Construct the headers dynamically based on the sensor name
        headers = [f'{sensor_name}_{i:04d}' for i in range(1, attributes_nb + 1)]
        
        # Construct the file path
        file_path = os.path.join('../data/raw/', sensor_name + '.txt')
        print(file_path )

        # Read the file with the dynamically generated headers
        df_sensor = pd.read_csv(file_path, delimiter='\t', names=headers, index_col=False)
        print('df_sensor shape', df_sensor.shape)
        
        #Apply the function to each row of the DataFrame
        df_sensor_agg = df_sensor.apply(calculate_time_domain_features, axis=1)
        df_sensor_agg.columns = [sensor_name +'_' + s for s in df_sensor_agg.columns]

        # Merge with the combined DataFrame using a left join
        if combined_df.empty:
            combined_df = df_sensor_agg
        else:
            combined_df = pd.merge(combined_df, df_sensor_agg, how='left', left_index=True, right_index=True)
                
    return combined_df
        