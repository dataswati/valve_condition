def get_predictions(X_test, model):

    """
    Get predictions and probabilities for the test dataset.

    Parameters:
    - X_test: array-like or pandas DataFrame, shape (n_samples, n_features)
      Test samples.
    - model: machine learning model object
      The model used for predictions. Must have predict and predict_proba methods.

    Returns:
    - y_pred: array-like, shape (n_samples,)
      Predictions for the test data.
    - y_proba: array-like, shape (n_samples,)
      Probability estimates for the positive class in binary classification.
    """

    # Predictions
    y_pred = model.predict(X_test)
    y_proba = model.predict_proba(X_test)[:, 1]  # Probabilities for ROC-AUC, applicable if binary classification
    
    return y_pred, y_proba


def train_GridCV(model, X_train, y_train, param_grid):
    """
    Train a model using GridSearchCV to find the best parameters.

    Parameters:
    - model: machine learning model object
      The model to be trained.
    - X_train: array-like or pandas DataFrame, shape (n_samples, n_features)
      Training data.
    - y_train: array-like, shape (n_samples,)
      Target values.
    - param_grid: dict or list of dictionaries
      Dictionary with parameters names (str) as keys and lists of parameter settings to try as values.

    Returns:
    - best_model: machine learning model object
      The best model found by GridSearchCV.
    """

    # Instantiate the GridSearchCV object
    grid_search = GridSearchCV(model, param_grid, cv=5, scoring='accuracy', n_jobs=-1)

    # Fit the model
    grid_search.fit(X_train, y_train)

    # Best model after CV
    best_model = grid_search.best_estimator_

    return best_model 

def get_eval(y_test, y_pred, best_params):
    """
    Print evaluation metrics for the test dataset predictions and the best parameters of the model.

    Parameters:
    - y_test: array-like, shape (n_samples,)
      True labels for the test data.
    - y_pred: array-like, shape (n_samples,)
      Predicted labels for the test data.
    - best_params: dict
      The best parameters for the model as found by GridSearchCV or similar methods.
    """

    # Evaluation
    accuracy = accuracy_score(y_test, y_pred)
    precision = precision_score(y_test, y_pred, average='weighted')
    recall = recall_score(y_test, y_pred, average='weighted')
    f1 = f1_score(y_test, y_pred, average='weighted')

    print(f"Best Model Parameters: {best_params}")
    print(f"Accuracy: {accuracy}")
    print(f"Precision: {precision}")
    print(f"Recall: {recall}")
    print(f"F1 Score: {f1}")

def save_model(output_file, model): 
    """
    Save the trained model to a file using pickle.

    Parameters:
    - output_file: str
      The path to the file where the model should be saved.
    - model: machine learning model object
      The model to be saved.
    """
    
    f_out = open(output_file, 'wb') 
    pickle.dump(model, f_out)
    f_out.close()